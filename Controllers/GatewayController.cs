﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using VTagPushDemoWebServer.Libs.Entities;
using System.Threading.Tasks;
using VTagPushDemoWebServer.Libs.Extensions;
using VTagPushDemoWebServer.Libs;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using Dapper;

namespace VTagPushDemoWebServer.Controllers
{
    [Route("api/[controller]")]
    public class GatewayController : Controller
    {
        private readonly SequenceHandler sequenceHandler;
        private readonly BeaconHandler beaconHandler;
        private readonly DataCache dataCache;
        private readonly ILogger<GatewayController> logger;

        public GatewayController(
            SequenceHandler sequenceHandler, 
            BeaconHandler beaconHandler, 
            DataCache dataCache,
            ILogger<GatewayController> logger)
        {
            this.sequenceHandler = sequenceHandler;
            this.beaconHandler = beaconHandler;
            this.dataCache = dataCache;
            this.logger = logger;
        }

        [HttpPost]
        [Authorize]
        public void UpdateGateway([FromQuery] string gatewayID, [FromQuery] string gatewayName, [FromQuery] Double latitude, [FromQuery] Double longitude, [FromQuery] string ipAddress)
        {
            //Gateway periodically sends this message up to the cloud with an update.
            string username = this.GetUsername();
            Console.WriteLine(string.Format("********************************************************UpdateGateway - gatewayID:{0},gatewayName:{1},ipAddress:{2},latitude:{3},longitude:{4},username:{5}", gatewayID, gatewayName, ipAddress, latitude, longitude, username));
        }

        [HttpPut]
        [Route("SecurityIndications")]
        [Authorize]
        public void Add([FromBody] SecurityIndication[] sis)
        {
            if (sis == null)
                return;
            ModelState.Validate();
            string username = this.GetUsername();            
            lock (dataCache)
            {
                //Gateway can be configured to push Security Indications in Batch mode in which case SecurityIndications are sent up in batches. If sent up in Batch, uses BeaconHandler to use intelligent processing of which Gateway each tag is closest to.
                Console.WriteLine(string.Format("*********************************************************Got Bulk SecurityIndications:Count={0}",sis.Length));
                foreach (SecurityIndication si in sis)
                {
                    //Console.WriteLine(string.Format("*********************************************************Got SecurityIndication:" + si.ToString()));
                    si.Username = username;
                    beaconHandler.AddSecurityIndication(si);
                }
            }
        }

        [HttpPut]
        [Route("SecurityIndication")]
        [Authorize]
        public void AddSingle([FromBody] SecurityIndication si)
        {
            ModelState.Validate();
            //Gateway can be configured to push Security Indications in Single mode in which case each SecurityIndication is immediately sent up.
            string username = this.GetUsername();
            si.Username = username;
            Console.WriteLine(string.Format("*********************************************************Got SecurityIndication:" + si.ToString()));
            /*
            //Insert into foreign db example..
            string connectionString = "Server=MyServer;Database=MyDB;User Id=MyUser;Password=MyPW;";     // I'll copy the My.. stuff from another conn string
            using (var conn = GetConnection(connectionString))
            {   
                //using dapper
                conn.Execute("INSERT INTO mytablename(att1,att2,att3) VALUES values "+
                    "(@TagID, @GatewayID, @RSSI)", new { si.TagID, si.GatewayID, si.RSSI });
            }
            */

        }

        [HttpPut]
        [Route("TagMovement")]
        [Authorize]
        public void AddTagMovement([FromBody] TagMovement tm)
        {
            //When a non-beacon V-Tag is moved it will start sending in movement reports. The V-Tag Gateway sends them up once a minute for 15 minutes.
            ModelState.Validate();
            if (sequenceHandler.RecentlySeen(tm.TagID, tm.SequenceNumber))
                return;
            string username = this.GetUsername();
            Console.WriteLine(string.Format("*********************************************************Got TagMovement:" + tm.ToString()));
        }

        [HttpPut]
        [Route("Alarm")]
        [Authorize]
        public async Task AddAlarm([FromBody] AlarmIndication alarmIndication)
        {
            ModelState.Validate();
            string username = this.GetUsername();
            Console.WriteLine("*******************************************************Alarm Indication[C] - "+alarmIndication.ToString()+" *******************************************************");
        }

        [HttpPost]
        [Route("SensorReading")]
        [Authorize]
        public void Add([FromBody] SensorReading sr)
        {
            //Once an hour each V-Tag will send a SensorReport in which has sensor statistical information.
            ModelState.Validate();
            if (sequenceHandler.RecentlySeen(sr.VTagID, sr.SequenceNumber))
                return;
            var req = HttpContext.Request;
            string username = this.GetUsername();
            Console.WriteLine(string.Format("*********************************************************Got SensorReading:" + sr.ToString()));
        }

        public SqlConnection GetConnection(string connStr)
        {
            var conn = new SqlConnection(connStr);
            conn.Open();
            return conn;
        }

    }
}
