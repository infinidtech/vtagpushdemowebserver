﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;

namespace VTagPushDemoWebServer.Libs
{
    public sealed class AuthorizationProvider : OpenIdConnectServerProvider
    {
        public AuthorizationProvider()
        {
        }

        private async Task<string[]> GetClientAppScopes(string clientId)
        {
            List<string> scopes = new List<string>() { OpenIdConnectConstants.Scopes.OpenId,
                OpenIdConnectConstants.Scopes.Email,
                OpenIdConnectConstants.Scopes.Profile,
                OpenIdConnectConstants.Scopes.OfflineAccess};
            return scopes.ToArray();
        }

        public override async Task HandleTokenRequest(HandleTokenRequestContext context)
        {            
            string[] scopes = await GetClientAppScopes(context.Request.ClientId);
            if (context.Request.IsClientCredentialsGrantType())
            {
                //V-Tag Gateways are expected to use Client Credentials Grant Type
                //At this point the request has already been validated.
                string customerID = "My Customer";

                var identity = new ClaimsIdentity(
                OpenIdConnectServerDefaults.AuthenticationScheme,
                OpenIdConnectConstants.Claims.Name,
                OpenIdConnectConstants.Claims.Role);
                // Note: the subject claim is always included in both identity and
                // access tokens, even if an explicit destination is not specified.

                identity.AddClaim(OpenIdConnectConstants.Claims.Subject, customerID,
                    OpenIdConnectConstants.Destinations.AccessToken,
                    OpenIdConnectConstants.Destinations.IdentityToken);

                // Create a new authentication ticket holding the user identity.
                var ticket = new AuthenticationTicket(
                    new ClaimsPrincipal(identity),
                    new AuthenticationProperties(),
                    OpenIdConnectServerDefaults.AuthenticationScheme);
                ticket.SetScopes(scopes.Intersect(context.Request.GetScopes()));
                ticket.SetResources("vtagpush_demo");

                context.Validate(ticket);
            }
        }

        public override async Task ValidateAuthorizationRequest(ValidateAuthorizationRequestContext context)
        {
            // Note: the OpenID Connect server middleware supports the authorization code, implicit and hybrid flows
            // but this authorization provider only accepts response_type=code authorization/authentication requests.
            // You may consider relaxing it to support the implicit or hybrid flows. In this case, consider adding
            // checks rejecting implicit/hybrid authorization requests when the client is a confidential application.
            if (!context.Request.IsAuthorizationCodeFlow())
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedResponseType,
                    description: "Only the authorization code flow is supported by this authorization server.");

                return;
            }

            // Note: to support custom response modes, the OpenID Connect server middleware doesn't
            // reject unknown modes before the ApplyAuthorizationResponse event is invoked.
            // To ensure invalid modes are rejected early enough, a check is made here.
            if (!string.IsNullOrEmpty(context.Request.ResponseMode) && !context.Request.IsFormPostResponseMode() &&
                                                                       !context.Request.IsFragmentResponseMode() &&
                                                                       !context.Request.IsQueryResponseMode())
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.InvalidRequest,
                    description: "The specified 'response_mode' is unsupported.");

                return;
            }

            // Retrieve the application details corresponding to the requested client_id.
            var application = new { context.RedirectUri };

            if (application == null)
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.InvalidClient,
                    description: "The specified client identifier is invalid.");

                return;
            }

            if (!string.IsNullOrEmpty(context.RedirectUri) &&
                !string.Equals(context.RedirectUri, application.RedirectUri, StringComparison.Ordinal))
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.InvalidClient,
                    description: "The specified 'redirect_uri' is invalid.");

                return;
            }

            context.Validate(application.RedirectUri);
        }

        public override async Task ValidateTokenRequest(ValidateTokenRequestContext context)
        {
            // Note: the OpenID Connect server middleware supports authorization code, refresh token, client credentials
            // and resource owner password credentials grant types but this authorization provider uses a safer policy
            // rejecting the last two ones. You may consider relaxing it to support the ROPC or client credentials grant types.


            if (context.Request.IsAuthorizationCodeGrantType())
            {
                bool clientOk = await ValidateClientId(context.Request.ClientId, context.Request.ClientSecret);
                if (!clientOk)
                {
                    context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                    description: "Client Id rejected.");
                    return;
                }
            }
            else if (context.Request.IsClientCredentialsGrantType())
            {
                bool clientOk = await ValidateClientId(context.Request.ClientId, context.Request.ClientSecret);
                if (!clientOk)
                {
                    context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                    description: "Client credentials rejected.");
                    return;
                }

            }
            else if (context.Request.IsRefreshTokenGrantType())
            {
                bool clientOk = await ValidateClientId(context.Request.ClientId, context.Request.ClientSecret);
                if (!clientOk)
                {
                    context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                    description: "Client credentials rejected.");
                    return;
                }
            }
            else
            {
                context.Reject(
                   error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                   description: "Only authorization code and client token grant types " +
                                "are accepted by this authorization server.");

                return;
            }

            context.Validate();
        }

        private async Task<bool> ValidateClientId(string clientId, string secret)
        {
            //you would really do some sort of db call here.
            if (clientId == "admin" && secret == "mypassword")
                return true;
            return false;

        }
       
    }
}
