﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VTagPushDemoWebServer.Libs.Entities;

namespace VTagPushDemoWebServer.Libs
{
    public class BeaconHandler
    {
        private const int BEACON_THRESHOLD_SECONDS = 10;
        private const int IDLE_BEACON_THRESHOLD_SECONDS = 30;
        public SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        private Dictionary<string, List<BeaconMessage>> beaconMessages = new Dictionary<string, List<BeaconMessage>>();
        private Dictionary<string, string> lastTagGatewayId = new Dictionary<string, string>();
        private Dictionary<string, DateTimeOffset> lastTagTimestamp = new Dictionary<string, DateTimeOffset>();
        private readonly Dictionary<string, GatewayCount> tagGatewayCount = new Dictionary<string, GatewayCount>();
        private readonly ILogger<BeaconHandler> logger;
        public BeaconHandler(ILogger<BeaconHandler> logger)
        {
            this.logger = logger;
        }

        #region Methods

        public void AddSecurityIndication(SecurityIndication securityIndication)
        {
            try
            {
                Semaphore.Wait();
                string nowString = DateTimeOffset.Now.ToString("yyyy-MM-dd hh:mm:ss");
                if (securityIndication.TagID == "0040B50040B5")
                    logger.LogError("************************************************************************************ Adding SecurityIndication. TagID=" + securityIndication.TagID + ",GatewayID=" + securityIndication.GatewayID + ", Received=" + securityIndication.ReceivedString + " and now=" + nowString + ", RSSI =" + securityIndication.RSSI);
                BeaconMessage message = GetCurrentBeaconMessage(securityIndication);
                message.AddSecurityIndication(securityIndication);
                //if (securityIndication.TagID == "0040B50040B5")
                //    System.Diagnostics.Debug.WriteLine("************************************************************************************ END Adding SecurityIndication. ");
            }
            finally
            {
                Semaphore.Release();
            }
        }

        public string GetLastTagGatewayID(string tagId)
        {
            return lastTagGatewayId.ContainsKey(tagId) ? lastTagGatewayId[tagId] : null;
        }

        public DateTimeOffset? GetLastTagTimestamp(string tagId)
        {
            DateTimeOffset? lastTimestamp = null;
            if (lastTagTimestamp.ContainsKey(tagId))
                lastTimestamp = lastTagTimestamp[tagId];
            return lastTimestamp;
        }

        public List<SecurityIndication> GetSecurityIndications()
        {
            try
            {
                Semaphore.Wait();
                //automatically filters out beacon messages that were weaker than other gateway' messages
                List<SecurityIndication> mySecurityIndications = new List<SecurityIndication>();
                foreach (string key in beaconMessages.Keys)
                {
                    List<BeaconMessage> tagBeaconMessages = beaconMessages[key];
                    bool done = false;
                    while (!done && tagBeaconMessages.Count > 0)
                    {
                        BeaconMessage tempMessage = tagBeaconMessages[0];
                        double seconds = (DateTimeOffset.Now - tempMessage.Received).TotalSeconds;
                        if (tempMessage.TagID == "0040B50040B5")
                            logger.LogError("**************** Get Security Indications For TagID 0040B50040B5. Received=" + tempMessage.ReceivedString + ", Seconds=" + seconds + ". TotalCount=" + tagBeaconMessages.Count);
                        if (seconds > IDLE_BEACON_THRESHOLD_SECONDS)
                        {
                            tagBeaconMessages.RemoveAt(0);
                            SecurityIndication winner = tempMessage.Winner;
                            lastTagGatewayId[winner.TagID] = winner.GatewayID;
                            lastTagTimestamp[winner.TagID] = DateTimeOffset.Now;

                            int count = SetTagGateway(winner.TagID, winner.GatewayID);
                            if (count > 3)
                            {
                                if (winner.TagID.Equals("0040B50040B5"))
                                    logger.LogError("****Got Winner: tagId " + winner.TagID + ", gatewayId=" + winner.GatewayID + ",received=" + winner.ReceivedString + ",rssi=" + winner.RSSI + ",seconds=" + seconds);
                                mySecurityIndications.Add(winner);
                            }
                            else
                            {
                                if (winner.TagID.Equals("0040B50040B5"))
                                    logger.LogError("****Got Winner But Count=" + count + ",  tagId" + winner.TagID + ", gatewayId=" + winner.GatewayID + ",received=" + winner.ReceivedString + ",rssi=" + winner.RSSI + ",seconds=" + seconds);
                                logger.LogDebug("****Got Winner But Count=" + count + ",  tagId" + winner.TagID + ", gatewayId=" + winner.GatewayID + ",received=" + winner.ReceivedString + ",rssi=" + winner.RSSI + ",seconds=" + seconds);
                            }
                        }
                        else
                            done = true;
                    }
                }
                return mySecurityIndications;
            }
            finally
            {
                Semaphore.Release();
            }
        }

        private BeaconMessage GetCurrentBeaconMessage(SecurityIndication securityIndication)
        {
            List<BeaconMessage> tagMessages = GetByTagId(securityIndication.TagID);
            BeaconMessage currentMessage = null;
            for (int i = 0; i < tagMessages.Count; i++)
            {
                BeaconMessage message = tagMessages[i];
                int seconds = (securityIndication.Received - message.Received).Seconds;
                decimal absSeconds = Math.Abs(seconds);
                if (absSeconds < BEACON_THRESHOLD_SECONDS)
                {
                    currentMessage = message;
                    if (securityIndication.TagID.Equals("0040B50040B5"))
                        logger.LogError("Found existing beacon message[" + i + "].seconds=" + seconds + " with received at " + message.ReceivedString + " for tagid " + message.TagID);
                    break;
                }
            }
            if (currentMessage == null)
            {
                if (securityIndication.TagID.Equals("0040B50040B5"))
                    logger.LogError("Adding new BeaconMessage for tagId " + securityIndication.TagID + " at time " + securityIndication.ReceivedString);
                currentMessage = new BeaconMessage() { TagID = securityIndication.TagID, Received = securityIndication.Received };
                tagMessages.Add(currentMessage);
            }
            return currentMessage;
        }

        private List<BeaconMessage> GetByTagId(String tagId)
        {
            if (!beaconMessages.ContainsKey(tagId))
            {
                beaconMessages.Add(tagId, new List<BeaconMessage>());
            }
            return beaconMessages[tagId];
        }

        public int SetTagGateway(string tagId, string gatewayId)
        {
            GatewayCount gatewayCount = null;
            if (!tagGatewayCount.ContainsKey(tagId))
            {
                gatewayCount = new GatewayCount(gatewayId, 0);
                tagGatewayCount.Add(tagId, gatewayCount);
            }
            else
            {
                gatewayCount = tagGatewayCount[tagId];

            }
            double seconds = (DateTimeOffset.Now - gatewayCount.LastBeacon).TotalSeconds;
            if (seconds > 3600)
            {
                //this beacon hasn't been seen for a long time so set the count back to 0
                gatewayCount.Count = 0;
            }
            gatewayCount.LastBeacon = DateTimeOffset.Now;
            if (gatewayCount.GatewayID == gatewayId)
            {
                gatewayCount.Count++;
            }
            else
            {
                gatewayCount.GatewayID = gatewayId;
                gatewayCount.Count = 1;
            }
            return gatewayCount.Count;
        }

        #endregion Methods

        public class BeaconMessage
        {
            public BeaconMessage()
            {

            }

            #region Properties

            public string TagID
            {
                get; set;
            }

            public DateTimeOffset Received
            {
                get; set;
            } = DateTimeOffset.Now;

            public string ReceivedString
            {
                get
                {
                    return Received.ToString("yyyy-MM-dd hh:mm:ss");
                }
            }

            private List<SecurityIndication> SecurityIndications { get; set; } = new List<SecurityIndication>();

            #endregion Properties

            #region Methods

            public SecurityIndication Winner
            {
                get
                {
                    //if (TagID == "0040B50040B5")
                    //    System.Diagnostics.Debug.WriteLine("************************************************************************************Choosing Winner. Count=" + SecurityIndications.Count);
                    SecurityIndication strongest = null;
                    foreach (SecurityIndication si in SecurityIndications)
                    {
                        //if (TagID == "0040B50040B5")
                        //    System.Diagnostics.Debug.WriteLine("************************************************************************************si.tagid=" + si.TagID + ", si.gatewayid=" + si.GatewayID + ", si.rssi=" + si.RSSI + " received="+si.ReceivedString);
                        if (strongest == null || si.RSSI > strongest.RSSI)
                            strongest = si;
                    }
                    return strongest;
                }
            }

            public void AddSecurityIndication(SecurityIndication si)
            {
                SecurityIndications.Add(si);
            }

            #endregion Methods

        }//BeaconMessage

        class GatewayCount
        {
            public GatewayCount(string gatewayID, int count)
            {
                GatewayID = gatewayID;
                Count = count;
            }

            public string GatewayID { get; set; }
            public int Count { get; set; }
            public DateTimeOffset LastBeacon = DateTimeOffset.Now;
        }

    }
}
