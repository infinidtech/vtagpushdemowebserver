﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using VTagPushDemoWebServer.Libs.Entities;

namespace VTagPushDemoWebServer.Libs
{
    /// <summary>
    /// BeaconProcessor is a helper class that does state analysis of the beacon messages being passed in from the BeaconHandler
    /// to see whether a database update is appropriate. It batch processes beacon messages perdiodically
    /// </summary>
    public class BeaconProcessor
    {
        private readonly DataCache dataCache;
        private readonly BeaconHandler beaconHandler;
        private readonly ILogger<BeaconProcessor> logger;

        Timer timer;
        public BeaconProcessor(DataCache dataCache,
            BeaconHandler beaconHandler,
            ILogger<BeaconProcessor> logger)
        {
            this.dataCache = dataCache;
            this.beaconHandler = beaconHandler;
            this.logger = logger;
        }

        public void Start()
        {
            timer = new Timer(new TimerCallback(DoWork), null, 10000, 5000);
        }

        public void Stop()
        {
        }

        public void DoWork(Object state)
        {
            try
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                List<SecurityIndication> securityIndications = beaconHandler.GetSecurityIndications();
                foreach (SecurityIndication current in securityIndications)
                {
                    bool updateTagLocation = false;
                    Tag tag = dataCache.GetTag(current.TagID);
                    if (tag == null)
                    {
                        tag = Tag.Parse(current);
                        dataCache.UpdateTag(tag);
                        updateTagLocation = true;
                    }
                    Gateway gateway = dataCache.GetGateway(current.GatewayID);
                    if (gateway == null)
                    {
                        gateway = new Gateway() { ID = current.GatewayID, Name = current.GatewayID };
                        dataCache.SetGatewayReader(current.GatewayID, gateway);
                    }
                    //compare asset location id with reader location id
                    if (tag != null && tag.GatewayID != gateway.ID)
                    {
                        //the location of our asset doesn't match the assigned location of the winner gateway so update
                        tag.GatewayID = gateway.ID;
                        updateTagLocation = true;

                    }
                    string lastWinnerGateway = dataCache.GetLastVTagGateway(current.TagID);
                    if (current.GatewayID != lastWinnerGateway)
                    {
                        //the winner gateway has changed since last beacon
                        updateTagLocation = true;
                    }
                    DateTimeOffset? beaconLastSeen = dataCache.GetVTagBeaconLastSeen(current.TagID);
                    bool beaconOld = beaconLastSeen == null;
                    if (beaconLastSeen != null)
                    {
                        double seconds = (DateTimeOffset.Now - beaconLastSeen.Value).TotalSeconds;
                        beaconOld = seconds > 3600;
                    }
                    if (beaconOld)
                    {
                        //Haven't seen this tag for quite a while so let's update db no matter what.
                        updateTagLocation = true;
                    }
                    dataCache.SetLastVTagGateway(current.TagID, current.GatewayID);
                    dataCache.UpdateBeaconLastSeen(current.TagID);
                    if (updateTagLocation)
                    {
                        Console.WriteLine(string.Format("********************************************************Updating Tag in DB. TagID={0}, GatewayID={1}", current.TagID, current.GatewayID));
                        //HERE IS WHERE YOU WOULD UPDATE YOUR DATABASE
                    }

                }
            }
            catch (Exception exc)
            {
                logger.LogError(new EventId(), exc, "Got error in BeaconProcessor:DoWork");
            }
            finally
            {
                timer.Change(30000, 30000);
            }
        }//DoWork

    }
}
