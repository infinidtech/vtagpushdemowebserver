﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs
{
    public class ConfigSettings
    {
        public string AuthServerUrl { get; set; }
        public string AuthServerPfxPath { get; set; }
        public string AuthServerPfxPassword { get; set; }

    }
}
