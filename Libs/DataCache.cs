﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VTagPushDemoWebServer.Libs.Entities;

namespace VTagPushDemoWebServer.Libs
{
    public class DataCache
    {
        Dictionary<string, Tag> vTagIdToTag = new Dictionary<string, Tag>();
        Dictionary<string, Gateway> gatewayIdToReader = new Dictionary<string, Gateway>();
        Dictionary<string, string> lastVTagGateway = new Dictionary<string, string>();
        Dictionary<string, DateTimeOffset> vTagBeaconLastSeen = new Dictionary<string, DateTimeOffset>();

        public DateTimeOffset? GetVTagBeaconLastSeen(string vTagID)
        {
            if (vTagBeaconLastSeen.ContainsKey(vTagID))
                return vTagBeaconLastSeen[vTagID];
            return null;
        }

        public void UpdateBeaconLastSeen(string vTagID)
        {
            vTagBeaconLastSeen[vTagID] = DateTimeOffset.Now;
        }

        public string GetLastVTagGateway(string vtagID)
        {
            return lastVTagGateway.ContainsKey(vtagID) ? lastVTagGateway[vtagID] : null;
        }

        public void SetLastVTagGateway(string vTagID, string gatewayID)
        {
            lastVTagGateway[vTagID] = gatewayID;
        }

        public Tag GetTag(string tagID)
        {
            if (vTagIdToTag.ContainsKey(tagID))
                return vTagIdToTag[tagID];
            return null;
        }

        public void UpdateTag(Tag tag)
        {
            if (tag == null)
                return;
            if (tag.TagID != null)
                vTagIdToTag[tag.TagID] = tag;           
        }

        public Gateway GetGateway(string gatewayID)
        {
            if (gatewayIdToReader.ContainsKey(gatewayID))
                return gatewayIdToReader[gatewayID];
            return null;
        }

        public void SetGatewayReader(string gatewayID, Gateway gateway)
        {
            gatewayIdToReader[gatewayID] = gateway;
        }


    }
}
