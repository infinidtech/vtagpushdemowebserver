﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class AlarmIndication
    {
        public string TagID { get; set; }
        public int SequenceNumber { get; set; }
        public string GatewayID { get; set; }
        public string ThresholdType { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return string.Format("TagID={0},SeqNum={1},GatewayID={2},ThresholdType={3},Message={4}", TagID, SequenceNumber, GatewayID, ThresholdType, Message);
        }
    }
}
