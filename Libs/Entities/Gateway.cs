﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class Gateway
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public DateTime LastSeen { get; set; }
        public string IpAddress { get; set; }
        public bool Inactive { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [JsonIgnore]
        public string Username { get; set; }        
    }
}
