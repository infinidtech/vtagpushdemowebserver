﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class SecurityIndication
    {
        public SecurityIndication()
        {
            Direction = "None";
        }

        // Constructor
        public SecurityIndication(string tagID)
        {
            TagID = tagID;
            AntennaID = -1;
            RSSI = 0;
            X = 0;
            Y = 0;
            Z = 0;
            PositionType = "Unknown";
            Direction = "None";
        }

        // Properties
        private string tagID = null;
        public string TagID
        {
            get
            {
                return tagID;
            }
            set
            {
                tagID = value.ToUpper();
            }
        }
        public long ReaderID { get; set; }
        public string GatewayID { get; set; }
        public int? SequenceNumber { get; set; }
        public int AntennaID { get; set; }
        public int RSSI { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public int? Z { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public double? Altitude { get; set; }
        public int? NumSatellites { get; set; }
        public DateTime? LastGpsFix { get; set; }
        public int? GpsAccuracy { get; set; }
        public string ParentVTag { get; set; }
        public string NearestFixed { get; set; }
        public string VTagType { get; set; }
        public string PositionType { get; set; }
        public string CustomFieldsType { get; set; }
        public string PositionReportingMode { get; set; }
        [JsonIgnore]
        public string Username { get; set; }

        public DateTimeOffset Received { get; set; } = DateTimeOffset.Now;

        public string ReceivedString
        {
            get
            {
                return Received.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        public Dictionary<string, object> CustomFields { get; set; } = new Dictionary<string, object>();

        public string Direction { get; set; }

        public override string ToString()
        {
            return string.Format("TagID:{0},GatewayID:{11},PositionReportingMode:{1},X:{2},Y:{3},Z:{4},Latitude:{5},Longitude:{6},NearestFixed:{7},VTagType:{8},ParentVTag:{9},PositionType:{10}",
                TagID, PositionReportingMode, X, Y, Z, Latitude, Longitude, NearestFixed, VTagType, ParentVTag, PositionType, GatewayID);
        }
    }
}
