﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class SensorReading
    {
        public SensorReading()
        {
        }
        public string VTagID { get; set; }
        public string SourceDistanceHops { get; set; }
        public int? TTL { get; set; }
        public decimal? MinimumTemperatureDegC { get; set; }
        public decimal? MaximumTemperatureDegC { get; set; }        
        public decimal? MaximumAccelerationG { get; set; }
        public decimal? BatteryVolts { get; set; }        
        public DateTimeOffset? LastGpsFix { get; set; }
        public int? NumSatellites { get; set; }
        public int? GpsAccuracy { get; set; }
        public string ParentVTag { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Altitude { get; set; }
        public string VTagType { get; set; }
        public string LocationConfidence { get; set; }
        public string NearestFixed { get; set; }
        public DateTimeOffset? Created { get; set; }
        public int? SequenceNumber { get; set; }
        public string GatewayID { get; set; }
        [JsonIgnore]
        public string Username { get; set; }

        public override string ToString()
        {
            return "VTagID:" + VTagID + ", GatewayID:" + GatewayID+",NearestFixed:"+NearestFixed+",ParentID:"+ParentVTag+",Latitude:"+Latitude+",Longitude:"+Longitude+",VTagType:"+VTagType+",BatteryVolts:"+BatteryVolts;
        }


    }
}
