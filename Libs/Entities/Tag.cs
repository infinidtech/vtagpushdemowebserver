﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VTagPushDemoWebServer.Libs.Entities;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class Tag
    {

        private string tagID = null;
        public string TagID
        {
            get
            {
                return tagID;
            }
            set
            {
                tagID = value.ToUpper();
            }
        }

        public DateTimeOffset? LastSatelliteFix { get; set; }
        public int? NumSatellites { get; set; }
        public int? Accuracy { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Altitude { get; set; }
        public string PositionType { get; set; }

        public string VTagType { get; set; }
        public string NearestFixed { get; set; }
        public int? MissedSatelliteFixes { get; set; }

        public DateTimeOffset? LastSeen { get; set; }
        public DateTimeOffset? LastMoved { get; set; }
        public DateTimeOffset? LastModified { get; set; }
        public DateTimeOffset? Created { get; set; }
        public string ParentID { get; set; }
        public string GatewayID { get; set; }
        public double? BatteryLevel { get; set; }
        public bool Inactive { get; set; }

        [JsonIgnore]
        public string Username { get; set; }
        public string PrintOut()
        {
            return "Tag ID:" + TagID + ",Latitude:" + Latitude + ",Longitude:" + Longitude + ",Altitude:" + Altitude + ", LastSeen=" + LastSeen?.ToString("s") + ", LastMoved=" + LastMoved?.ToString("s") + ", LastModified=" + LastModified?.ToString("s");
        }

        public static Tag Parse(SecurityIndication securityIndication)
        {
            Tag tag = new Tag()
            {
                TagID = securityIndication.TagID,
                GatewayID = securityIndication.GatewayID,
                Accuracy = securityIndication.GpsAccuracy,
                Altitude = securityIndication.Altitude,
                LastSatelliteFix = securityIndication.LastGpsFix,
                LastSeen = securityIndication.Received,
                Latitude = securityIndication.Latitude,
                Longitude = securityIndication.Longitude,
                NearestFixed = securityIndication.NearestFixed,
                PositionType = securityIndication.PositionType,
                VTagType = securityIndication.VTagType
            };
            return tag;

        }
    }
}
