﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Entities
{
    public class TagMovement
    {
        private string tagID = null;
        public string TagID
        {
            get
            {
                return tagID;
            }
            set
            {
                tagID = value.ToUpper();
            }
        }

        public DateTimeOffset? LastGpsFix { get; set; }
        public int? NumSatellites { get; set; }
        public int? GpsAccuracy { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Altitude { get; set; }
        public string PositionType { get; set; }

        public string VTagType { get; set; }
        public string NearestFixed { get; set; }        
        
        public string ParentVTag { get; set; }
        public string GatewayID { get; set; }
        
        public int? SequenceNumber { get; set; }
        
        public string PrintOut()
        {
            return ToString();
        }

        public override string ToString()
        {
            return "Tag ID:" + TagID + "GatewayID:" + GatewayID + ",Latitude:" + Latitude + ",Longitude:" + Longitude + ",Altitude:" + Altitude + ",NearestFixed:" + NearestFixed + ",ParentID:" + ParentVTag;
        }
    }
}
