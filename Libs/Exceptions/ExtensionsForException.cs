﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Exceptions
{
    public static class ExtensionsForException
    {
        public static string FullMessage(this Exception exc)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(exc.Message);
            sb.AppendLine(exc.StackTrace);
            if (exc.InnerException != null)
                sb.AppendLine("Inner Exception: " + exc.InnerException.Message);
            else
                sb.AppendLine("No Inner Exception: ");
            return sb.ToString();
        }
    }
}
