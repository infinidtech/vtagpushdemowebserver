﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Extensions
{
    public static class AuthenticationBuilderExtensions
    {
        //2C49E540330B4B21D8EE3D8A402AD6204E8F783D <-- pfx thumbprint..
        //740927b3b2b560722040fc6c9874b7e54a3bc89f <-- pub cert thumbprint
        public static AuthenticationBuilder AddJwtBearer(this AuthenticationBuilder authBuilder, string schemeName, string authority, bool validateAudience = true)
        {
            try
            {
                authBuilder = authBuilder.AddJwtBearer(schemeName, o =>
                {
                    // You also need to update /wwwroot/app/scripts/app.js
                    o.Authority = authority;
                    o.BackchannelHttpHandler = new HttpClientHandler { ServerCertificateCustomValidationCallback = delegate { return true; } };
                    o.Audience = "vtagpush_demo";
                    o.RequireHttpsMetadata = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        // Clock skew compensates for server time drift.
                        // We recommend 5 minutes or less:
                        ClockSkew = TimeSpan.FromMinutes(5),
                        // Specify the key used to sign the token:
                        //IssuerSigningKey = new X509SecurityKey(certificate),                            
                        RequireSignedTokens = true,
                        // Ensure the token hasn't expired:
                        RequireExpirationTime = true,
                        ValidateLifetime = true,
                        RequireAudience = true,
                        // Ensure the token audience matches our audience value (default true):
                        ValidateAudience = validateAudience,
                        // Ensure the token was issued by a trusted authorization server (default true):
                        ValidateIssuer = false,
                    };
                    o.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async context =>
                        {
                            // Add the access_token as a claim, as we may actually need it
                            var accessToken = context.SecurityToken as JwtSecurityToken;
                            if (accessToken != null)
                            {
                            }
                        },
                        OnMessageReceived = context =>
                        {

                            return Task.CompletedTask;
                        },
                        OnAuthenticationFailed = context =>
                        {

                            return Task.CompletedTask;
                        },
                        OnChallenge = context =>
                        {

                            return Task.CompletedTask;
                        },
                        OnForbidden = context =>
                        {

                            return Task.CompletedTask;
                        },
                    };
                });


            }
            catch (Exception exc)
            {

            }
            return authBuilder;
        }
    }
}
