﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Extensions
{
    public static class ControllersExtension
    {
        public static string GetUsername(this Controller controller)
        {
            string authenticationText = controller.HttpContext.Request.Headers["Authorization"].ToString();
            int firstSpace = authenticationText.IndexOf(" ");
            string tokenText = authenticationText.Substring(firstSpace + 1);
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtSecurityTokenHandler.ReadJwtToken(tokenText);
            var membershipId = jwtToken.Claims.FirstOrDefault(p => p.Type == "MembershipId");
            var username = membershipId?.Value.ToLower();
            return username;
        }
    }
}
