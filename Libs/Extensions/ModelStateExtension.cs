﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VTagPushDemoWebServer.Libs.Exceptions;

namespace VTagPushDemoWebServer.Libs.Extensions
{
    public static class ModelStateExtension
    {
        public static string GetErrors(this ModelStateDictionary modelState)
        {
            string messages = string.Join("; ", modelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
            return messages;
        }

        public static void Validate(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                StringBuilder sb = new StringBuilder();
                foreach(var val in modelState.Values)
                {
                    foreach(var err in val.Errors)
                    {
                        string errMsg = err.Exception != null ? err.Exception.Message : err.ErrorMessage;
                        sb.AppendLine(errMsg);
                    }                  
                }
                string messages = string.Join("; ", modelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                throw new AppException("Model is not valid:" + sb.ToString());
            }
        }
    }
}
