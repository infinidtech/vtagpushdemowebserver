﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Extensions
{
    public static class StringExtensions
    {

        public static List<string> ConvertEach(this List<string> items, Func<string, string> func)
        {
            List<string> newList = new List<string>();
            for (int i = 0; i < items.Count; i++)
            {
                newList.Add(func(items[i]));
            }
            return newList;
        }

        public static List<string> ToUpper(this List<string> items)
        {
            return ConvertEach(items, p => p.ToUpper());
        }


        public static List<string> SplitCsv(this string csvList, bool nullOrWhitespaceInputReturnsNull = false)
        {
            if (string.IsNullOrWhiteSpace(csvList))
                return nullOrWhitespaceInputReturnsNull ? null : new List<string>();

            return csvList
                .TrimEnd(',')
                .Split(',')
                .AsEnumerable<string>()
                .Select(s => s.Trim())
                .ToList();
        }

        public static bool IsNullOrWhitespace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }
    }
}
