﻿using VTagPushDemoWebServer.Libs.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;

namespace VTagPushDemoWebServer.Libs.Filters
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        ILogger<ApiExceptionFilter> logger;
        public ApiExceptionFilter(ILogger<ApiExceptionFilter> logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            ApiError apiError = null;
            if (context.Exception is UnauthorizedAccessException)
            {
                logger.LogInformation("Unauthorized Request");
                apiError = new ApiError("Unauthorized Access");
                context.HttpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;

                // handle logging here
            }
            else if(context.Exception is AppException)
            {
                logger.LogError("AssetWorx Exception: {0}"+context.Exception.FullMessage());
                apiError = new ApiError(context.Exception.Message);
                context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else if (context.Exception is NotAuthorizedException)
            {
                logger.LogError("AssetWorx Unauthorized Request");
                apiError = new ApiError(context.Exception.Message);
                context.HttpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
            }
            else {
                // Unhandled errors
#if !DEBUG                
                var msg = context.Exception is AppException ? context.Exception.GetBaseException().Message : "An unhandled error occurred.";
                string stack = context.Exception is AppException ? context.Exception.StackTrace : null;
#else                                
                var msg = context.Exception.GetBaseException().Message;
                string stack = context.Exception.StackTrace;
#endif
                apiError = new ApiError(msg);
                apiError.detail = stack;

                logger.LogError("General Exception: {0}" + context.Exception.FullMessage());

                context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

                // handle logging here
            }

            // always return a JSON result
            context.Result = new JsonResult(apiError);

            base.OnException(context);
        }
    }

   

    public class ApiError
    {
        public string message { get; set; }
        public bool isError { get; set; }
        public string detail { get; set; }

        public ApiError(string message)
        {
            this.message = message;
            isError = true;
        }
        
    }
}
