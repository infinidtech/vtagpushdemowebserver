﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs
{
    public class HttpHelper
    {
        public enum Method { GET, PUT, POST, DELETE };



        private static async Task<HttpContent> GetHttpContent(Method method, string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {           
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    HttpResponseMessage response = null;
                    string parms = String.Join("&", parameters.Select(p => p.Name + "=" + p.Value));
                    string reqUri = parms.Length > 0 ? resource + "/" + action + "?" + parms : resource + "/" + action;
                    string json = JsonConvert.SerializeObject(body);
                    switch (method)
                    {
                        case Method.GET:
                            response = await client.GetAsync(reqUri);
                            break;
                        case Method.POST:
                            response = await client.PostAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.PUT:
                            response = await client.PutAsync(reqUri, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                            break;
                        case Method.DELETE:
                            response = await client.DeleteAsync(reqUri);
                            break;
                    }
                    if (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        return response.Content;
                    }
                    else
                    {
                        throw await HandleBadResponse(response, resource, action);
                    }
                }
            }
            catch (Exception exc)
            {
                string message = exc.InnerException == null ? exc.Message : exc.Message + Environment.NewLine + exc.InnerException.Message;
                //_logger.LogError("Got Error:" + message);
                throw new Exception(message);
            }
        }

        protected static async Task<S> DoHttp<S>(Method method, string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, baseUrl, resource, action, body, parameters);
            string json = await content.ReadAsStringAsync();
            if (!string.IsNullOrWhiteSpace(json))
            {
                S result = JsonConvert.DeserializeObject<S>(json);
                return result;
            }
            return default(S);
        }

        public static async Task<string> DoHttp(Method method, string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            var content = await GetHttpContent(method, baseUrl, resource, action, body, parameters);
            string json = await content.ReadAsStringAsync();
            return json;
        }

        public static async Task<S> DoGet<S>(string baseUrl, string resource, string action, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.GET, baseUrl, resource, action, null, parameters);
        }

        public static async Task<string> DoGet(string baseUrl, string resource, string action, params QueryParam[] parameters)
        {
            return await DoHttp(Method.GET, baseUrl, resource, action, null, parameters);
        }

        public static async Task<S> DoPost<S>(string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.POST, baseUrl, resource, action, body, parameters);
        }

        public static async Task<string> DoPost(string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp(Method.POST, baseUrl, resource, action, body, parameters);
        }

        public static async Task DoDelete(string baseUrl, string resource, string action, object body = null, params QueryParam[] parameters)
        {
            await DoHttp(Method.DELETE, baseUrl, resource, action, body == null ? "" : body, parameters);
        }

        public static async Task<S> DoPut<S>(string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp<S>(Method.PUT, baseUrl, resource, action, body == null ? "" : body, parameters);
        }

        public static async Task<string> DoPut(string baseUrl, string resource, string action, object body, params QueryParam[] parameters)
        {
            return await DoHttp(Method.PUT, baseUrl, resource, action, body == null ? "" : body, parameters);
        }

        private static async Task<Exception> HandleBadResponse(HttpResponseMessage resp, string resource, string action)
        {
            string content = await resp.Content.ReadAsStringAsync();
            return new Exception(string.Format("Got bad response. in calling resource {0}/{1}. HTTP Code:{2}. Error:{3}", resource, action, resp.StatusCode, content));
        }

        public class QueryParam
        {
            public readonly string Name, Value;
            public QueryParam(string name, object value)
            {
                this.Name = name;
                string val = value == null ? "" : value.ToString();
                this.Value = WebUtility.UrlEncode(val);
            }
        }
    }
}
