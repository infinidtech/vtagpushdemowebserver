﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs
{
    public class PositionType
    {
        public const string GPS = "GPS";
        public const string GPSNOFIX = "GPS No Fix";
        public const string Fixed = "Fixed";
        public const string Unknown = "Unknown";
    }
}
