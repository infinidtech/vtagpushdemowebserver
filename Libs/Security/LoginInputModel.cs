﻿
namespace VTagPushDemoWebServer.Libs.Security
{
    public class LoginInputModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsUser { get; set; } = true;
    }
}
