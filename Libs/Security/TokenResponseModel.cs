﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs.Security
{
    public class TokenResponseModel
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
    }
}
