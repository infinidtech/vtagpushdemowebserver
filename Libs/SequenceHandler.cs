﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VTagPushDemoWebServer.Libs
{
    public class SequenceHandler
    {

        #region Properties

        private Dictionary<string, DateTime> _activeTagMessageSeen = new Dictionary<string, DateTime>();
        protected Dictionary<string, DateTime> ActiveTagMessageSeen
        {
            get { return _activeTagMessageSeen; }
            set
            {
                _activeTagMessageSeen = value;
            }
        }

        #endregion Properties

        public bool RecentlySeen(string tagId, int? sequenceNumber)
        {
            if (!sequenceNumber.HasValue || sequenceNumber.Value == -1)
                return false;

            // If there are multiple gateways, there is no way to prevent some duplicate packets from different gateways
            // If duplicates are seen, they should be rejected
            bool bRecentlySeen = false;

            string msg_identifier = tagId + ":" + sequenceNumber;
            if (ActiveTagMessageSeen.ContainsKey(msg_identifier))
            {
                TimeSpan ts = DateTime.Now.Subtract(ActiveTagMessageSeen[msg_identifier]);
                if (ts.TotalMinutes < 30.0)
                {
                    bRecentlySeen = true;
                }
            }

            // Occasionally trim old records
            System.Random random = new Random();
            if (random.NextDouble() < 0.05)
            {
                List<string> old_records = new List<string>();
                DateTime cutoff = DateTime.Now.AddHours(-1.0);
                old_records = (from r in ActiveTagMessageSeen
                               where r.Value < cutoff
                               select r.Key).ToList<string>();
                foreach (string old_record in old_records)
                {
                    ActiveTagMessageSeen.Remove(old_record);
                }
            }

            // Add a new record to the dictionary
            ActiveTagMessageSeen[msg_identifier] = DateTime.Now;
            return bRecentlySeen;
        }

        // Is the current movement report superceded by a later movement report due to delays in the network
        protected bool TagMovementReportSuperceded(string tagId, string sequenceNumber)
        {
            bool bMovementReportSuperceded;

            if (sequenceNumber == null || sequenceNumber.Length == 0 || sequenceNumber == "-1")
                return false;

            string msg_identifier = "MOV:" + tagId + ":" + sequenceNumber;
            bMovementReportSuperceded = false;

            // Tag movement messages may arrive out of order - check for a match against an incremented sequence number
            for (int increment = 1; increment < 4; increment++)
            {
                string out_of_order = "MOV:" + tagId + ":" + incremented_sequence_number(sequenceNumber, increment);
                if (ActiveTagMessageSeen.ContainsKey(out_of_order))
                {
                    TimeSpan ts = DateTime.Now.Subtract(ActiveTagMessageSeen[out_of_order]);
                    if (ts.TotalMinutes < 30.0)
                    {
                        bMovementReportSuperceded = true;
                    }
                }
            }
            // Add a new record to the dictionary
            ActiveTagMessageSeen[msg_identifier] = DateTime.Now;
            return bMovementReportSuperceded;
        }

        // Increment a sequence number
        protected string incremented_sequence_number(string sequenceNumber, int increment)
        {
            int current_sequence_number = Convert.ToInt32(sequenceNumber, 16);
            int new_sequence_number = current_sequence_number + increment;
            if (new_sequence_number >= 256)
            {
                new_sequence_number -= 256;
            }
            return String.Format("{0:X2}", new_sequence_number);
        }
    }
}
