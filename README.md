﻿V-Tag Push Demo Web Server Instructions:

This demo program is a console application which runs on .Net Core. It is compatible with Windows, Macintosh, Linux, as well as Docker containers. 

You can run the app in Visual Studio, Visual Studio Code, or directly from the terminal. Below are instructions for each option:

Running from Terminal:
--------------------------------
Install .Net Core SDK from https://dotnet.microsoft.com/download
Open your terminal and go to the VTagPushDemoWebServer folder
type "dotnet restore"
type "dotnet run"

Running from Visual Studio:
--------------------------------
Make sure Visual Studio 2019 or later is installed. The free Visual Studio Community edition will work fine.
Open up the VTagPushDemoWebServer.csproj file which will open the project in Visual Studio. Hit F5 to run the program.

Running from Visual Studio Code:
--------------------------------
First download and instal Visual Studio Code from https://code.visualstudio.com/
Install .Net Core SDK from https://dotnet.microsoft.com/download
Open the VTagPushDemoWebServer project folder in VS Code
Install Recommended extensions
Open Integrated Terminal under View and type "dotnet restore"
From terminal type "dotnet run"
To Debug, click on the Bug icon on the left. Select .Net Core Attach and press the Green start button. Select your process from the list to attach the debugger.

How to point your V-Tag Gateway to the Demo Web Server:
---------------------------------
default client id and secret is "admin" and "mypassword"
V-Tag Gateway Setup Guide - https://drive.google.com/open?id=1USeh51jI-417d0Tv7hluWJKYBIBu6O4q
V-Tag Gateway Push Swagger definitions - https://app.swaggerhub.com/apis/InfinID-Technologies/VTag-Gateway-Push/1.0
