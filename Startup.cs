﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json.Serialization;
using VTagPushDemoWebServer.Libs;
using VTagPushDemoWebServer.Libs.Extensions;
using VTagPushDemoWebServer.Libs.Filters;

namespace VTagPushDemoWebServer
{
    public class Startup
    {
        ConfigSettings configSettings;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader() 
                    );
            });

            services.Configure<ConfigSettings>(Configuration.GetSection("ConfigSettings"));
            var myServiceProvider = services.BuildServiceProvider();
            var configSettings = myServiceProvider.GetService<IOptions<ConfigSettings>>();
            this.configSettings = configSettings.Value;

            var serviceProvider = WireUpDependencyInjection(this.configSettings, services);

            services.AddMvc().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddOptions();

            IdentityModelEventSource.ShowPII = true;
            AuthenticationBuilder authBuilder = services.AddAuthentication(options =>
            {
                //options.DefaultScheme = "ServerCookie";
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            });
            authBuilder = authBuilder.AddCookie("ServerCookie", options =>
            {
                options.Cookie.Name = CookieAuthenticationDefaults.CookiePrefix + "ServerCookie";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = new PathString("/account/login");
                options.LogoutPath = new PathString("/account/logout");
            });

            authBuilder = authBuilder.AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, "http://localhost:5000", false);

            authBuilder = authBuilder.AddOpenIdConnectServer(options =>
            {
                options.ProviderType = typeof(AuthorizationProvider);

                // Enable the authorization and token endpoints.                
                options.AuthorizationEndpointPath = "/connect/authorize";
                options.LogoutEndpointPath = "/connect/logout";
                options.TokenEndpointPath = "/connect/token";
                options.UserinfoEndpointPath = "/connect/userinfo";
                options.AllowInsecureHttp = true;
                options.ApplicationCanDisplayErrors = true;
                options.AccessTokenLifetime = TimeSpan.FromMinutes(60);
                options.IdentityTokenLifetime = TimeSpan.FromMinutes(60);
                // options.RefreshTokenFormat = new ISecureDataFormat<AuthenticationTicket>();
                // Note: to override the default access token format and use JWT, assign AccessTokenHandler:
                //
                options.AccessTokenHandler = new JwtSecurityTokenHandler
                {
                    InboundClaimTypeMap = new Dictionary<string, string>(),
                    OutboundClaimTypeMap = new Dictionary<string, string>()
                };
                //
                // Note: when using JWT as the access token format, you have to register a signing key.
                //
                // You can register a new ephemeral key, that is discarded when the application shuts down.
                // Tokens signed using this key are automatically invalidated and thus this method
                // should only be used during development:
                //                
                //options.SigningCredentials.AddEphemeralKey();
                //
                // On production, using a X.509 certificate stored in the machine store is recommended.
                // You can generate a self-signed certificate using Pluralsight's self-cert utility:
                // https://s3.amazonaws.com/pluralsight-free/keith-brown/samples/SelfCert.zip
                //
                FileStream fileStream = File.OpenRead(configSettings.Value.AuthServerPfxPath);
                options.SigningCredentials.AddCertificate(fileStream, configSettings.Value.AuthServerPfxPassword);
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.StatusCode = 500; // or another Status accordingly to Exception Type
                    context.Response.ContentType = "application/json";

                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null)
                    {
                        var ex = error.Error;
                        logger.LogError(0, "Exception passed up to Startup:" + ex.Message, ex);
                        await context.Response.WriteAsync(new ErrorDto()
                        {
                            Code = 500,
                            Message = ex.Message
                        }.ToString(), System.Text.Encoding.UTF8);
                    }
                });
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }//Configure

        private ServiceProvider WireUpDependencyInjection(ConfigSettings configSettings, IServiceCollection services)
        {
            services.ConfigurePOCO<ConfigSettings>(Configuration.GetSection("ConfigSettings"));
            services.AddSingleton<BeaconHandler, BeaconHandler>();
            services.AddSingleton<BeaconProcessor>();
            services.AddSingleton<SequenceHandler>();
            services.AddSingleton<DataCache>();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<ApiExceptionFilter>();
            services.AddSingleton<AuthorizationProvider>();
            return services.BuildServiceProvider();
        }
    }    
}
